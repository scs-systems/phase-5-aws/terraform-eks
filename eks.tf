module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  // version = "18.29.0"
  // version = "19.10.0"  // 9/3/2023
  version = "19.10.0"
  cluster_endpoint_private_access = false
  cluster_endpoint_public_access  = true  // gitlab TF build container needs access
 
  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  eks_managed_node_groups = {
    default = {
      key_name      = var.support_key_pair_name
      min_size       = 2
      max_size       = var.instance_count
      desired_size   = var.instance_count
      instance_types = [var.instance_type]
      vpc_security_group_ids = [
        aws_security_group.cluster_security_group.id
      ]
    }
  }
  tags = {
    Name = var.cluster_name
  }
}
