data "aws_availability_zones" "available" {}

// name = module.eks.cluster_id  - eks module 18.29.0
// RJ NOTE - need to comment this bit out on first run - must be better solution !
// data "aws_eks_cluster" "cluster" {
//  name = module.eks.cluster_name
// }

// name = module.eks.cluster_id  - eks module 18.29.0
data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_name
}