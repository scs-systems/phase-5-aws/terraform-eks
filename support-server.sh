#!/bin/bash
SSH_KEY=$1
AWS_ACCESS_KEY_ID=$2
AWS_SECRET_ACCESS_KEY=$3
AWS_REGION=$4
CLUSTER_NAME=$5
PAYLOAD_REPO=$6

echo '###############################'
echo '# Start support-server.sh - mainly about installs'
echo '###############################'
sudo dnf makecache
# git in order to get the payload
sudo dnf install git -y
# Unzip needed for AWS Client install
sudo dnf install unzip -y
echo '###############################'
echo '# STAGE: Install AWS CLI'
cd /tmp
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o awscliv2.zip
unzip -o awscliv2.zip
sudo ./aws/install --update
aws --version
# Kubectl
echo '###############################'
echo '# STAGE: Install kubectl'
sudo curl "https://s3.us-west-2.amazonaws.com/amazon-eks/1.24.10/2023-01-30/bin/linux/amd64/kubectl" -o "/usr/local/bin/kubectl"
sudo chmod 755 /usr/local/bin/kubectl
ls -l /usr/local/bin/kubectl
kubectl version
echo '###############################'
echo '# STAGE: Create ssh private key'
echo "$SSH_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
ls -l ~/.ssh/id_rsa
echo '###############################'
echo '# STAGE: Create aws cli and kubectl config files'
aws configure set aws_access_key_id "$AWS_ACCESS_KEY_ID"
aws configure set aws_secret_access_key "$AWS_SECRET_ACCESS_KEY"
aws configure set region "$AWS_REGION"
aws eks update-kubeconfig --region "$AWS_REGION" --name "$CLUSTER_NAME"
ls -l ~/.kube/config
ls -lR ~/.aws
aws eks describe-cluster --name terraform-eks --query 'cluster.status'
kubectl get nodes
echo '###############################'
echo '# STAGE: Download and apply the payload'
rm -rf /tmp/payload
git clone "$PAYLOAD_REPO" /tmp/payload
sh /tmp/payload/payload.sh >/tmp/payload.log 2>&1
echo '###############################'
echo "Finished support-server.sh"
echo '###############################'