terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.58.0"    // 9/3/2023
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.18.1"    // 9/3/2023
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "15.9.0"    // 9/3/2023
    }
  }
}
