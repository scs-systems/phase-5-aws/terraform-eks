// https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html
// https://wiki.centos.org/Cloud/AWS#Finding_AMI_ids

//servers.tf
resource "aws_instance" "test-ec2-instance" {
  tags = {
    Name = "Cluster support"
  }
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = var.support_key_pair_name
  // vpc_security_groups as "security_groups" caused EC2 rebuild - bug ?
  vpc_security_group_ids = [
    aws_security_group.cluster_security_group.id,
    aws_security_group.ingress-all-test.id
  ]
  subnet_id = module.vpc.public_subnets[0]
}

resource "aws_eip" "ip-test-env" {
  instance = aws_instance.test-ec2-instance.id
  vpc      = true
}

resource "null_resource" "ssh_target" {
  // depends_on = [aws_eip.ip-test-env]
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = aws_eip.ip-test-env.public_ip
    private_key = var.ssh_key
  }

  // I want this to run every time
  // see: https://www.kecklers.com/terraform-null-resource-execute-every-time/ ( missing "=")
  triggers = {
    build_number = "${timestamp()}"
  }

  provisioner "file" {
    source      = "support-server.sh"      # terraform machine
    destination = "/tmp/support-server.sh" # remote machine
  }

  // RJ note - this lot get put into a script and copied to server = eg: /tmp/terraform_474126179.sh
  // After execution the script gets "nulled out" - size 0 bytes
  provisioner "remote-exec" {
    inline = [
      "sh /tmp/support-server.sh \"${var.ssh_key}\" \"${var.AWS_ACCESS_KEY_ID}\" \"${var.AWS_SECRET_ACCESS_KEY}\" \"${var.region}\" \"${var.cluster_name}\" \"${var.payload}\" >/tmp/support-server.log 2>&1",
    ]
  }
}