// EKS and Support-Server

variable "region" {
  default     = "eu-west-2"
  description = "AWS region"
}

variable "support_key_pair_name" {
  default     = "richjam"
  description = "EC2 Key pair name"
}


// EKS cluster ONLY

variable "cluster_name" {
  default     = "terraform-eks"
  description = "EKS Cluster name"
}

variable "cluster_version" {
  default     = "1.25"
  description = "Kubernetes version"
}

variable "instance_type" {
  default     = "t2.small"
  description = "EKS node instance type"
}

variable "instance_count" {
  default     = 2
  description = "EKS node count"
}

// Support-Server ONLY

variable "ami_id" {
  // default     = "ami-0454011e44daf8e6d" // Centos 9 - new format ssh key
  // default     = "ami-0418c980c296f36ce" // Centos 8 -fails now 13/9/2024
  default     = "ami-035cecbff25e0d91e"
  description = "AMI ID"
}

variable "ssh_user" {default     = "ec2-user"}

variable "payload" {
  default = "https://gitlab.com/scs-systems/phase-5-aws/infra/ansible-test-server.git"
  description = "https url of payload git repo"
}

// This will come from gitlab emvironment variable = TF_VAR_ssh_key
variable "ssh_key" {default     = "junk"}
variable "AWS_ACCESS_KEY_ID" {default     = "junk"}
variable "AWS_SECRET_ACCESS_KEY" {default     = "junk"}