# remember export TF_VAR_GITLAB_TOKEN=?????
variable "GITLAB_TOKEN" {
  type = string
}

# Configure the GitLab Provider
provider "gitlab" {
  token = var.GITLAB_TOKEN
}


// The following example creates a GitLab Agent for Kubernetes in a given project
// creates a token and install the `gitlab-agent` Helm Chart.
// (see https://gitlab.com/gitlab-org/charts/gitlab-agent)
data "gitlab_project" "this" {
  path_with_namespace = "scs-systems/phase-5-aws/infra/terraform-eks"
}
// Optionally, configure the agent as described in
// https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#create-an-agent-configuration-file


resource "gitlab_cluster_agent" "this" {
  project = data.gitlab_project.this.id
  name    = "gitlab-agent"
}


resource "gitlab_cluster_agent_token" "this" {
  project     = data.gitlab_project.this.id
  agent_id    = gitlab_cluster_agent.this.agent_id
  name        = "my-agent-token"
  description = "Token for the my-agent used with `gitlab-agent` Helm Chart"
}

resource "helm_release" "gitlab_agent" {
  name             = "gitlab-agent"
  namespace        = "gitlab-agent"
  create_namespace = true
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-agent"
  // version          = "1.2.0"
  // version          = "1.11.0"     // 9/3/2023
  version          = "1.11.0"

  set {
    name  = "config.token"
    value = gitlab_cluster_agent_token.this.token
  }
  set {
    name  = "image.tag"
    value = "v15.9.0"
  }
  set {
    name  = "config.kasAddress"
    value = "wss://kas.gitlab.com"
  }
}
