# README 16/9/2024-1 richjam
# EKS Environment - with test/support server

# Acks

https://gitlab.com/gitlab-org/configure/examples/gitlab-terraform-eks

This repository contains sample code for creating an AWS Elastic Kubernetes Service (EKS) cluster using [GitLab Infrastructure as Code](https://docs.gitlab.com/ee/user/infrastructure/), and connecting it to GitLab with the [GitLab agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/).

For more information on how to use it, please refer to the [official docs](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_eks_cluster.html).


# RJ update
I've made changes - will go through them later:
* gitlab agent changes
* also installing a support server. I wanted a server outside the cluser that I could use as a "bastion" host.  SSH into that one - and use it for runing kubectl commands - or for SSH'ing into the nodes.