// EKS

output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}

output "cluster_status" {
  description = "Status of the EKS cluster. One of CREATING, ACTIVE, DELETING, FAILED."
  value       = module.eks.cluster_status
}


// Support-Server

output "aws_eip_public_ip" {
  description = "Public IP of aws_eip"
  value       = aws_eip.ip-test-env.public_ip
}
